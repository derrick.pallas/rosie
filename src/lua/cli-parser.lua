-- -*- Mode: Lua; -*-                                                                             
--
-- cli-parser.lua
--
-- © Copyright Jamie A. Jennings 2018, 2019.
-- © Copyright IBM Corporation 2017.
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHORS: Jamie A. Jennings, Kevin Zander

----------------------------------------------------------------------------------------
-- Parser for command line args
----------------------------------------------------------------------------------------

local p = {}

-- create Parser
function p.create(rosie)
   local argparse = assert(rosie.import("argparse"), "failed to load argparse package")
   local parser = argparse("rosie", "Rosie " .. rosie.attributes.ROSIE_VERSION)
   parser:add_help("--help")
   parser:require_command(false)
   parser:flag("--verbose", "Output additional messages")
   :default(false)
   :action("store_true")
   parser:option("--rpl", "Inline RPL statements")
   :args(1)
   :count("*") -- allow multiple RPL statements
   :target("statements") -- set name of variable index (args.statements)
   parser:option("-f --file", "Load an RPL file")
   :args(1)
   :count("*") -- allow multiple loads of a file
   :target("rpls") -- set name of variable index (args.rpls)

   local output_choices={}
   for k,v in pairs(rosie.encoders) do
      if type(k)=="string" then table.insert(output_choices, k); end
   end
   local output_choices_string = table.concat(output_choices, ", ")

   local function validate_output_encoder_choice(a)
      -- validation of argument, will fail if not in choices array
      for j=1,#output_choices do
	 if a == output_choices[j] then
	    return a
	 end
      end
      return nil
   end

   local trace_style_choices = {"json", "full", "condensed"}
   local trace_style_choices_string = table.concat(trace_style_choices, ", ")

   local function validate_trace_style_choice(a)
      -- validation of argument, will fail if not in choices array
      local output_choices = trace_style_choices
      for j=1,#output_choices do
	 if a == output_choices[j] then
	    return a
	 end
      end
      return nil
   end

   parser:option("--norcfile", "Skip initialization file")
   :args(0)
   :target("norcfile")				    -- args.norcfile
   :default(false)
   
   parser:option("--rcfile", "Initialization file to read")
   :args(1)
   :target("rcfile")				    -- args.rcfile
   :default(false)
   
   parser:option("--libpath", "Directories to search for rpl modules")
   :args(1)
   :target("libpath")				    -- args.libpath
   :default(false)

   parser:option("--colors", "Color/pattern assignments for color output")
   :args(1)
   :target("colors")				    -- args.colors
   :default(false)

   -- target variable for commands
   parser:command_target("command")
   local commands = {}

   -- version command
   commands.version = parser:command("version")
   :description("Print rosie version")
   -- help command
   commands.help = parser:command("help")
   :description("Print this help message")
   -- config command
   commands.info = parser:command("config")
   :description("Print rosie configuration information")
   -- list command
   commands.patterns = parser:command("list")
   :description("List patterns, packages, and macros")
   commands.patterns:argument("filter")
   :description("List all names that have substring 'filter'")
   :default("*")
   :args(1)
   -- grep command
   commands.grep = parser:command("grep")
   :description("In the style of Unix grep, match the pattern anywhere in each input line")
   commands.grep:option("-o --output", "Output style, one of: " .. output_choices_string)
   :convert(validate_output_encoder_choice)
   :args(1) -- consume argument after option
   :target("encoder")
   -- match command
   commands.match = parser:command("match")
   :description("Match the given RPL pattern against the input")
   commands.match:option("-o --output", "Output style, one of: " .. output_choices_string)
   :convert(validate_output_encoder_choice)
   :args(1) -- consume argument after option
   :target("encoder")
   -- repl command
   commands.repl = parser:command("repl")
   :description("Start the read-eval-print loop for interactive pattern development and debugging")
   -- test command
   commands.test = parser:command("test")
   :description("Execute pattern tests written within the target rpl file(s)")
   commands.test:argument("filenames", "RPL filenames")
   :args("+")
   -- expand command
   commands.expand = parser:command("expand")
   :description("Expand an rpl expression to see the input to the rpl compiler")
   commands.expand:argument("expression")
   :args('+')
   -- trace command
   commands.trace = parser:command("trace")
   :description("Match while tracing all steps (generates MUCH output)")
   commands.trace:option("-o --output", "Output style, one of: " .. trace_style_choices_string)
   :convert(validate_trace_style_choice)
   :args(1) -- consume argument after option
   :default("condensed")
   :target("encoder")

   -- TEMPORARY match command that loads the pattern from a (compiled rplx) file
   commands.rplxmatch = parser:command("rplxmatch")
   :description("Match the using the compiled pattern stored in the argument (an rplx file)")
   commands.rplxmatch:option("-o --output", "Output style, one of: " .. output_choices_string)
   :convert(validate_output_encoder_choice)
   :args(1) -- consume argument after option
   :target("encoder")

   for name, cmd in pairs(commands) do
      cmd:add_help("--help")
   end

   -- Common flags and arguments across match, trace, and grep
   for _, cmd in ipairs{commands.match, commands.trace, commands.grep, commands.rplxmatch} do
      -- match/trace/grep flags (true/false)
      cmd:flag("-w --wholefile", "Read the whole input file as single string")
      :default(false)
      :action("store_true")
      cmd:flag("-a --all", "Output non-matching lines to stderr")
      :default(false)
      :action("store_true")
      cmd:flag("-F --fixed-strings", "Interpret the pattern as a fixed string, not an RPL pattern")
      :default(false)
      :action("store_true")

      -- match/trace/grep arguments (required options)
      cmd:argument("pattern", "RPL pattern")
      cmd:argument("filename", "Input filename")
      :args("+")
      :default("-")			      -- in case no filenames are passed, default to stdin
      :defmode("arg")			      -- needed to make the default work
   end

   -- Common flags across match, and grep
   for _, cmd in ipairs{commands.match, commands.grep, commands.rplxmatch} do
      -- match/trace/grep flags (true/false)
      cmd:flag("--time", "Time each match, writing to stderr after each output")
      :default(false)
      :action("store_true")
   end
   
   return parser
end

return p

