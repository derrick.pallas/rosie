# Rosie Pattern Language Documentation

## Language Reference

The [Rosie Pattern Language Reference](rpl.md) defines the pattern language
apart from the Rosie implementation.  

## Unit tests

Unit tests are written as RPL comments and are described in the
[Unit Test Documentation](unittest.md). 


## Using the CLI

Some command line [examples](../extra/examples/README.md) are available, and
others can be found in blog posts on the [Rosie home page](https://rosie-lang.org).

See also an [html version](man/rosie.1.html) of the Rosie CLI
[man page](man/rosie.1). 


## Using the REPL

See the [REPL documentation](doc/repl.md).


## Using the API

Use Rosie in your own programs!  

If you are looking for language bindings for **librosie**, you'll find them in
the [Rosie Community / Clients](https://gitlab.com/rosie-community/clients)
group.

If you are writing a language binding, or improving one, see the
[**librosie** API documentation](librosie.md).


## Project extras
- [Syntax highlighting](../extra) for some editors 
- Sample [docker files](../extra/docker) for various distros.
- Some interesting [quotes about regex](quotes.txt) and related topics

