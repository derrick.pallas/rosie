# Tale of a DSL

	Just sit right back and you’ll hear a tale
	A tale of a DSL
		Born in a battle with regexes
		A tale I now shall tell
    
	We had regexes in Python code, and node and Scala, too
	Someone wrote in PHP; `git blame` won't tell me who
		We were searching through unstructured text, where regex use is king
		But with all the different regex kinds, we couldn’t share a thing
    
	So many bugs and mismatches, but too much code to chuck it
	I met with the entire team, ready to say...
		“I know everyone uses regexes for this but they really don’t meet our requirements,
		so we should look for another approach and I’ll invent one if I have to
		(which is exactly what I ended up doing)”
    
	And I almost ended with a swear word, but chose instead to duck it.

