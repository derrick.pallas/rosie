# Notes on Ahead-of-Time Compilation

Sunday, October 21, 2018

## Current situation derives from lpeg

Early versions of Rosie used `lpeg` as a starting point, and the `lpeg` compiler
as a second stage.  Rosie's RPL compiler produces `lpeg` patterns, internally
represented as trees, as an intermediate language.  Building patterns using the
`lpeg` operators (concatenation, repetition, choice, etc.) results in copying of
the constituent trees.  (See the functions in `lptree.c` that call `memcpy`).

The way `lpeg` was designed, patterns are stored in Lua variables, but when new
patterns are composed out of existing patterns, as in:

```lua
Lua 5.3.4  Copyright (C) 1994-2017 Lua.org, PUC-Rio
> lpeg = require 'lpeg'
> a = lpeg.P('a')
> b = lpeg.P('b')
> p = a * a * b
> p:match('aab')
4
> p:match('ab')
nil
> 
``` 

the existing patterns, `a` and `b` in this example, are copied into (not
referenced by) the composite, `p`.  There are good reasons for this
implementation decision.  The `lpeg` library is aimed, apparently, at small
patterns, where the space required by all of this copying is not problematic.
Unanticipated, perhaps, was the notion that users would build large libraries of
patterns, thus compounding the effect of all that copying.

On the other hand, `lpeg` supports recursive grammars.  It is not possible to
copy the definition of a single recursive rule or a set of mutually recursive
rules.  The matching virtual machine provides `call` and `return` instructions
to enable recursive grammars.  Each reference to a rule will result in a `call`
in the generated code.  (Note that references to patterns defined outside the
grammar are copied into a rule's definition.)  Since the matching vm already
uses a stack for backtracking, the implementations of `call` and `return` are
trivial.

It is possible to use the `lpeg` notion of a grammar in order to build
non-recursive patterns, such as this alternate version of the pattern `p` from
the earlier example:

```lua
> g = {a = lpeg.P('a'), b = lpeg.P('b')}
> g[1] = lpeg.V('a') * lpeg.V('a') * lpeg.V('b')
> gp = lpeg.P(g)
> gp:match('aab')
4
> 
> gp:match('ab')
nil
>
``` 

The pattern `gp` contains, in its internal representation, only one copy of the
tree for pattern `a`.  The resulting `lpvm` code will contain only one copy of
the instructions for `a`, which is expected to be invoked via `call`. 


### Using call/return for RPL pattern libraries

The `lpeg` package supports only grammars with a single entry point.  Thus, an
`lpeg` grammar is insufficient to implement an RPL library, in which we need an
entry point for each (named) pattern (e.g. `net.ipv4`, `net.ipv6`).

Yet, we want the property that a pattern like `net.ipv4 net.ipv4` contains only
one copy of `net.ipv4`.  Indeed, such a pattern should "call" `net.ipv4` as a
"subroutine" twice, instead of containing two copies of it.  We will build on
the starting point that `lpeg` provides, and design a library representation for
RPL which encodes named patterns as vm code that is designed to be called.

None of these ideas are new, of course.  Compilers for programming languages
create function and method definitions that use a call/return mechanism.  And
they can *inline* those definitions under certain conditions, in an attempt to
increase run-time performance (at the cost of executable size) by removing the
cost of the `call` and `return` instructions.

Saving and restoring registers and other global state comprises much of the
call/return cost in a CPU.  In the Rosie matching vm, which was derived from
`lpeg`, there is no state to save or restore.  While there remains a cost to
executing `call` and `return` in our vm, we can choose to inline small patterns
to reduce the accumulated impact of those costs.


## Design of a Static Compiled RPL Package

The representation of a *static compiled RPL package* has these parts:

0. [Meta] Meta-data (see below);
1. [Code] Vector of instructions for the matching vm;
2. [Ktable] A table of "capture names" indexed by small integers; and
3. [EP] A table mapping each pattern name in the library to a pattern structure containing:
    1. its entry point in the code (an index into the instruction vector)
	2. source file and position (?)
	2. whether or not it is an alias (?)
	3. default color for display (?)

Package meta-data includes:
- The default prefix name for the module
- The Rosie vm version required to execute the code
- The name, timestamp, and size in bytes of the source file

While the vm `call` instruction specifies a location to jump to, we need a
symbolic version of `call` for our compiled RPL packages.  The target (jump
destination) of a symbolic call is an index into the entry-point (EP) table,
effectively an indirect jump.

### Using a Static Compiled RPL Package

Assume an environment that maps pattern names to pattern structures, as
described previously.  Such an environment could be trivially constructed by
reading a file containing a *static compiled RPL package*.

When compiling a new pattern, `p`, that references by name a pattern, `q` in
such an environment, the reference compiles to a `call q` instruction.  The code
generated for the new pattern must be executed in a vm that can access the
referenced code.  Of course, the pattern `q` in this environment may reference
`r` in the same environment, and `r` may have dependencies of its own.

Clearly, in order to be able to match using the pattern `p`, we will need the
transitive closure of all of its dependencies.

Our notion of a *static* package is that it should be closed under the
dependency relationship for all the patterns it contains.  In other words, the
patterns defined in a *static compiled RPL package* have no external
dependencies. 

### Static versus Dynamic Packages

The design for *static compiled RPL packages* contains no information about
external dependencies.  Any dependencies that existed at the source level have
been compiled into the package representation, and are not needed at run-time.
This is analogous to static linking of executables.

We may consider the alternative, in which *dynamic compiled RPL packages*
contain references to external dependencies, such as other *compiled RPL
packages*. 

When a pattern, `p`, refers to a pattern in a *dynamic* package, that package
and all of its dependencies must be available at run-time.  The analogy to
dynamic linking of executables should be clear, and with it the attendant
pitfalls.  Not only must the dependencies be available at run-time, we would
like them to be versioned so that we are sure that we are getting the correct
implementation.

Today we have no requirements that would be solved by dynamic compiled RPL
packages. 


## Design of a Single Pattern Representation

The primary Rosie/RPL run-time scenario is the matching of a single pattern
against some input, where the input is an array of bytes.  (Recall that, in
Rosie, the UTF-8 encoding of Unicode characters has been compiled out, so that
the matching vm needs only to know about bytes.)

The matching vm takes as arguments a pattern, `pat`, and an input, `data`.  How
should the pattern be represented?

### Current approach

The current vm requires an instruction vector; implicit is that it should begin
executing at instruction 0, and that all branching instructions (including
`call`) have absolute target addresses.  An address is an index into the
instruction vector.

One option is to provide the vm with what it expects today: a "complete program"
for a pattern.  We can do that by first compiling the pattern provided by the
user, `p`, which may contain symbolic references to other patterns.  Resolving
those references means constructing environments containing the referenced
names.  Perhaps those environments will be constructed by reading from files, or
by compiling other RPL source code.

We can construct a "complete program" for `p` by a process resembling static
linking.  We append together the instructions for `p` and those for each pattern
in the environment.  Then we replace symbolic calls to targets like `q` and `r`
with direct calls to their final locations in the instruction vector.

The resulting "program" is a single instruction vector with absolute branch
targets, designed to be executed starting at instruction 0 (assuming we placed
the code for `p` first).  This instruction vector, together with its KTable,
form `pat`.

### Indirect Call Option

Alternatively, we can compile `p` into its own instruction vector, whose
instructions reference its own Ktable.  We can augment the matching vm such that
it understands symbolic call targets.  Perhaps we will introduce a new opcode,
`xcall` (for "external call"), that takes an *external target* as an
argument. (The name `xcall` is borrowed from the DIBOL language, where it was
used for a similar purpose, but in DIBOL source code.)

An external target is a pair *(iv, addr)*: an instruction vector and an absolute
address in that vector.  The `xcall` instruction will push onto the stack a
similar pair, consisting of the current instruction vector and address.

The approach requires no run-time linking or address relocation.  However, it
may create demands on the processor data cache that are absent with the static
linking approach.  Still, this approach is easy to implement.  If it proves to
be cache-unfriendly, we can easily implement static linking instead.

## More changes to lpeg

### It's rpeg already, anyway

At the time of this writing (October, 2018), we have renamed the `lpeg`
submodule in the Rosie project's development branch (on GitLab) to `rpeg` due to
the significant amount of alterations we have already made.

Among them:
- Replaced many Lua data structures with new implementations in C;
- Factored the run-time into a separate compilation unit; now, a matching engine
  can be deployed as a stand-alone unit, with no dependence on Lua, and
  requiring only a compiled pattern and input to run;
- Added new instructions and capture types;
- Changed the instruction coding scheme;
- Removed many Lua-specific capabilities, including function captures and table
  captures.

### Changes to enable ahead-of-time compilation

We can enhance `rpeg` as described above, independently of enabling file-based
persistence of compiled patterns and packages.

**Create a pattern struct**

The existing C struct for patterns is now insufficient.  A compiled pattern
contains a Ktable and an Instruction vector.  Before compilation, it contains a
Ktable and a tree data structure representing the expression.

Note: In the current code, the tree data structure for a pattern `q` is needed
in order to compile a pattern `p` that refers to `q`.  After implementing the
design described here, this is no longer a requirement.

The new pattern structure will include:
- Instruction vector (code)
- Ktable (captures)
- List of environments (dependencies)

**Implement a new vm instruction**

The Instruction vector will contain `xcall` instructions which index into the
table of environments and further to specific instructions in those
environments. 

The part of an environment needed at runtime is simply:
- Instruction vector (code) for all the patterns contained therein
- Ktable (captures)
- List of environments (dependencies)

In other words, at run-time, the structures for patterns and environments are
identical, despite their different uses.  A pattern executes starting at
instruction 0, and (possibly) contains external references to other
environments. 

While our initial implementation has *static compiled RPL packages* in mind, we
note here that, *mutatis mudandis*, our implementation supports *dynamic*
packages as well (because each environment may refer to patterns in its own
dependency list).

### Optimizations

Once the new implementation of patterns that use `xcall` is working, we can
consider some optimizations.

**Peephole**

Peephole optimization is likely to improve run-time performance, if we use it on
the combined instruction vectors of `p` and all of its dependencies.  When is
this optimization appropriate?  Perhaps only when we want to save `p` to a file
for reuse.  This use case optimizes for `p` but, by modifying the code of its
dependencies, e.g. `q`, prevents direct use of `q`.  That is, the complete
program for `p` will lack an entry point for `q`.

**Instruction Vector Allocation**

Memory locality affects performance, which suggests an experiment in which the
code for `p` and all of its dependencies are copied into a contiguous memory
block before entering the vm.  This arrangement, as opposed to having `p` and
its environment in separately allocated spaces, may provide a modest performance
boost.  We could couple with this memory layout a change to `p` wherein every
`xcall` becomes a simple `call`.

If `p` contains many `call`/`xcall` instructions to far destinations in the
instruction vector, then the cache demands posed by a single contiguous
instruction vector may be similar, or possibly greater, than those imposed when
`p` and its environment are stored separately.

**Converting VM Addresses to Native Addresses**

Consider an vector of instructions which have absolute branch targets (indices
into the instruction vector).  We could replace those targets with actual
pointers in the C sense, i.e. native memory addresses.  That would remove an
address calculation (of the form *base + index*) from every branch; and this vm
branches often.

The instruction format currently allocates one 32-bit word for a branch target.
One implementation for using native addresses would be to add an opcode that
takes 2 32-bit operands and concatenates them into a 64-bit native address
(pointer).  However, the operation to put together the pointer may be as costly
as the original pointer calculation.

The alternative to storing the pointer in 2 32-bit words is to use a single
64-bit word, but the alignment requirements for a struct containing such a value
would expand the size of the instruction vector for all instructions, even the
ones that do not need a 64-bit operand.




